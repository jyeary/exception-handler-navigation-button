/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.jsf.exception.handler;

import java.util.Iterator;
import java.util.logging.Logger;
import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class GeneralExceptionHandler extends ExceptionHandlerWrapper {

    private ExceptionHandler parent;
    private static final Logger LOG = Logger.getLogger(GeneralExceptionHandler.class.getName());

    public GeneralExceptionHandler(final ExceptionHandler parent) {
        this.parent = parent;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return parent;
    }

    @Override
    public void handle() throws FacesException {
        for (Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator(); i.hasNext();) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext eqec = (ExceptionQueuedEventContext) event.getSource();
            Throwable throwable = eqec.getException();

            if (throwable instanceof FacesException) {
                FacesContext fc = FacesContext.getCurrentInstance();
                ExternalContext ec = fc.getExternalContext();
                HttpServletRequest request = (HttpServletRequest) ec.getRequest();
                String originalRequestURI = request.getRequestURI();
                String encodedURL = ec.encodeRedirectURL(originalRequestURI, null);
                ec.getSessionMap().put("com.bluelotussoftware.jsf.exception.handler.GeneralExceptionHandler.URL", encodedURL);
            }
        }
        parent.handle();
    }
}