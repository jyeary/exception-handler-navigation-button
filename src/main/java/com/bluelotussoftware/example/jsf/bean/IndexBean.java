/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example.jsf.bean;

import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 */
@ManagedBean
@RequestScoped
public class IndexBean {
    
    private Object o;
    
    public IndexBean() {
    }
    
    public String npe() {
        return o.toString();//NPE
    }
    
    public String navigate() throws IOException {
        String redirectURL = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("com.bluelotussoftware.jsf.exception.handler.GeneralExceptionHandler.URL");
        FacesContext.getCurrentInstance().getExternalContext().redirect(redirectURL);
        return null;
    }
}
